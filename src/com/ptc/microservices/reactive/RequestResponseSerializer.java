package com.ptc.microservices.reactive;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import com.sm.javax.query.Patterns;
import com.sm.javax.query.PropertyArray;
import com.sm.javax.query.PropertyObject;
import com.sm.javax.query.PropertyValue;

public class RequestResponseSerializer
{
	private static Gson DefaultGson = null;
	
	private static GsonBuilder DefaultGsonBuilder = null;
	
	public static GsonBuilder GetDefaultGsonBuilder () 
	{
		if (DefaultGsonBuilder == null) {
			DefaultGsonBuilder = new GsonBuilder ()
					.registerTypeAdapter (Throwable.class, new ThrowableSerializer ())
					.registerTypeAdapter (Throwable.class, new ThrowableDeserializer ())
					.registerTypeAdapter (PropertyValue.class, new PropertyValueSerializer ())
					.registerTypeAdapter (PropertyValue.class, new PropertyValueDeserializer ())
					.registerTypeAdapter (PropertyObject.class, new PropertyObjectSerializer ())
					.registerTypeAdapter (PropertyObject.class, new PropertyObjectDeserializer ());
		}
		return DefaultGsonBuilder;
	}
	
	public static Gson GetDefaultGson ()
	{
		if (DefaultGson == null) {
			DefaultGson = GetDefaultGsonBuilder ().create ();
		}
		return DefaultGson;
	}
	
	// =====================================================================================
	private static final String MESSAGE_FIELD = "message";
	private static final String CAUSE_FIELD = "cause";
	private static final String EXCEPTION_FIELD = "exception";

	private static class ThrowableSerializer implements JsonSerializer<Throwable>
	{
		@Override
		public JsonElement serialize (Throwable src, Type type, JsonSerializationContext context)
		{
			JsonObject res = new JsonObject ();
			String msg = null;
			if (src == null) {
				msg = "Unknown error";
				src = new Exception (msg);
			}
			else {
				msg = src.getMessage ();
				if (msg == null) {
					msg = src.getClass ().getName ();
				}
			}
			res.add (MESSAGE_FIELD, new JsonPrimitive (msg)); 
			if (src.getCause () != null) {
				res.add (CAUSE_FIELD, new JsonPrimitive (String.valueOf (src.getCause ())));
			}
			res.add (EXCEPTION_FIELD, new JsonPrimitive (src.getClass ().getName ()));
			return res;
		}
	}

	// =====================================================================================
	private static class ThrowableDeserializer implements JsonDeserializer<Throwable>
	{
		@Override
		public Throwable deserialize (JsonElement src, Type type, JsonDeserializationContext context)
				throws JsonParseException
		{
			if (src == null || !src.isJsonObject ()) {
				return null;
			}
			JsonObject json = src.getAsJsonObject ();
			String message = json.get (MESSAGE_FIELD).getAsString ();
			Throwable cause = (json.has (CAUSE_FIELD) ? new Throwable (json.get (CAUSE_FIELD).getAsString ()) : null); 
			Throwable ex = null;
			if (json.has (EXCEPTION_FIELD)) {
				String exname = json.get (EXCEPTION_FIELD).getAsString ();
				try {
					Class<?> exclass = Class.forName (exname);
					try {
						Constructor<?> constr = exclass.getConstructor (String.class, Throwable.class);
						ex = (Throwable) constr.newInstance (message, cause);
					}
					catch (NoSuchMethodException | SecurityException er) {
						try {
							Constructor<?> constr = exclass.getConstructor (String.class);
							ex = (Throwable) constr.newInstance (message);
						}
						catch (NoSuchMethodException | SecurityException e) {
							Constructor<?> constr = exclass.getConstructor ();
							ex = (Throwable) constr.newInstance ();
						}
					}
				}
				catch (ClassNotFoundException | NoSuchMethodException | SecurityException | 
						InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) 
				{
					message = exname + ": " + message;
				}
			}
			if (ex == null) {
				ex = new Throwable (message, cause);
			}
			return ex;
		}
	}

	// =====================================================================================
	public static JsonElement toGson (PropertyObject value)
	{
		JsonObject obj = null;
		if (value != null) {
			obj = new JsonObject ();
			for (Entry<String, PropertyValue> entry : value) {
				obj.add (entry.getKey (), toGson (entry.getValue ()));
			}
		}
		return obj;
	}
	
	public static JsonElement toGson (PropertyValue value) 
	{
		JsonElement res = null;
		if (value != null) {
			if (value.isObject ()) {
				res = toGson ((PropertyObject) value); 
			}
			else if (value.isArray ()) {
				res = toGson ((PropertyArray) value);
			}
			else if (value.isRegExp ()) {
				res = new JsonPrimitive (value.toString ());
			}
			else if (value.isBoolean ()) {
				res = new JsonPrimitive (value.getBooleanValue ());
			}
			else if (value.isDouble ()) {
				res = new JsonPrimitive (value.getDoubleValue ());
			}
			else if (value.isInteger ()) {
				res = new JsonPrimitive (value.getIntValue ());
			}
			else if (value.isLong ()) {
				res = new JsonPrimitive (value.getLongValue ());
			}
			else if (value.isString ()) {
				res = new JsonPrimitive (value.getStringValue ());
			}
		}
		return res;
	}
	
	public static JsonArray toGson (PropertyArray value) 
	{
		JsonArray arr = null;
		if (value != null) {
			arr = new JsonArray ();
			for (PropertyValue elem : value) {
				arr.add (toGson (elem));
			}
		}
		return arr;
	}
	
	private static class PropertyValueSerializer implements JsonSerializer<PropertyValue>
	{
		@Override
		public JsonElement serialize (PropertyValue src, Type type, JsonSerializationContext context)
		{
			return toGson (src);
		}
	}

	private static class PropertyObjectSerializer implements JsonSerializer<PropertyObject>
	{
		@Override
		public JsonElement serialize (PropertyObject src, Type type, JsonSerializationContext context)
		{
			return toGson (src);
		}
	}

	// =====================================================================================
	public static PropertyObject fromGson (JsonObject value)
	{
		PropertyObject obj = null;
		if (value != null) {
			obj = new PropertyObject ();
			for (Entry<String, JsonElement> entry : value.entrySet ()) {
				obj.put (entry.getKey (), fromGson (entry.getValue ()));
			}
		}
		return obj;
	}
	
	public static PropertyArray fromGson (JsonArray value) 
	{
		PropertyArray arr = null;
		if (value != null) {
			ArrayList<PropertyValue> valueList = new ArrayList<> ();
			for (JsonElement val : value) {
				valueList.add (fromGson (val));
			}
			arr = new PropertyArray (valueList);
		}
		return arr;
	}
	
	public static PropertyValue fromGson (JsonElement value)
	{
		PropertyValue res = null;
		if (value != null) {
			if (value.isJsonPrimitive ()) {
				JsonPrimitive pval = value.getAsJsonPrimitive ();
				if (pval.isBoolean ()) {
					res = PropertyValue.create (pval.getAsBoolean ());
				}
				else if (pval.isNumber ()) {
					try {
						res = PropertyValue.create (pval.getAsDouble ());
					}
					catch (NumberFormatException ex1) {
						try {
							res = PropertyValue.create (pval.getAsInt ());
						}
						catch (NumberFormatException ex2) {
							try {
								res = PropertyValue.create (pval.getAsLong ());
							}
							catch (NumberFormatException ex3) {
								res = PropertyValue.create (pval.getAsString ());
							}
						}
					}
				}
				else if (pval.isString ()) {
					String strVal = pval.getAsString ();
					Pattern template = Patterns.parseRegularExpression (strVal);
					if (template == null) {
						res = PropertyValue.create (strVal);
					}
					else {
						res = PropertyValue.create (template);
					}
				}
			}
			else if (value.isJsonObject ()) {
				res = fromGson (value.getAsJsonObject ());
			}
			else if (value.isJsonArray ()) {
				res = fromGson (value.getAsJsonArray ());
			}
		}
		return res;
	}
	
	private static class PropertyValueDeserializer implements JsonDeserializer<PropertyValue>
	{
		@Override
		public PropertyValue deserialize (JsonElement src, Type type, JsonDeserializationContext context)
				throws JsonParseException
		{
			return fromGson (src);
		}
		
	}
	
	private static class PropertyObjectDeserializer implements JsonDeserializer<PropertyObject>
	{
		@Override
		public PropertyObject deserialize (JsonElement src, Type type, JsonDeserializationContext context)
				throws JsonParseException
		{
			return (PropertyObject) fromGson (src);
		}
		
	}
	
}
