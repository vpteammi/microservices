package com.ptc.microservices.reactive;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class ReactiveService
{
	private HashMap<String, RequestHandler<?, ?>> commandHandlers;
	private Supplier<Scheduler> schedulerSupplier;

	protected ReactiveService ()
	{
		super ();
		commandHandlers = new HashMap<> ();
	}

	protected Scheduler scheduler ()
	{
		return (schedulerSupplier == null) ? Schedulers.computation () : schedulerSupplier.get ();
	}

	public void setSchedulerSupplier (Supplier<Scheduler> schedulerSupplier)
	{
		this.schedulerSupplier = schedulerSupplier;
	}

	// ------------------------------------------------------------------------------
	protected <REQ_PARAM extends JsonSerializable, RESP_PARAM extends JsonSerializable> ReactiveService registerMethod (
			String methodName, RequestHandler<REQ_PARAM, RESP_PARAM> handler)
	{
		commandHandlers.put (methodName, handler);
		return this;
	}

	// ------------------------------------------------------------------------------
	public <REQ_PARAM extends JsonSerializable, RESP_PARAM extends JsonSerializable> ReactiveService registerCheckedMethod (
			String methodName, CheckedFunction<REQ_PARAM, RESP_PARAM> handler, Type requestType)
	{
		return registerMethod (methodName, new SynchRequestHandler<REQ_PARAM, RESP_PARAM> (handler, requestType));
	}

	public <REQ_PARAM extends JsonSerializable, RESP_PARAM extends JsonSerializable> ReactiveService registerMethod (
			String methodName, Function<REQ_PARAM, RESP_PARAM> handler, Type requestType)
	{
		return registerMethod (methodName, new SynchRequestHandler<REQ_PARAM, RESP_PARAM> (handler, requestType));
	}

	// ------------------------------------------------------------------------------
	public <REQ_PARAM extends JsonSerializable, RESP_PARAM extends JsonSerializable> ReactiveService registerMethod (
			String methodName, Consumer<ServiceRequest<REQ_PARAM, RESP_PARAM>> handler, Type requestType)
	{
		return registerMethod (methodName, new AsynchRequestHandler<REQ_PARAM, RESP_PARAM> (handler, requestType));
	}

	RequestHandler<?, ?> getRequestHandler (String name)
	{
		return commandHandlers.get (name);
	}

	// ------------------------------------------------------------------------------
	public static interface CheckedFunction<T, R>
	{
		R apply (T t) throws Throwable;
	}

	// ------------------------------------------------------------------------------
	abstract class RequestHandler<REQ_PARAM extends JsonSerializable, RESP_PARAM extends JsonSerializable>
	{
		public final Type requestType;

		public RequestHandler (Type requestType)
		{
			super ();
			this.requestType = requestType;
		}

		public abstract void processRequest (ServiceMessage<REQ_PARAM> request,
				Consumer<ServiceResponse<RESP_PARAM>> responseHandler);

		public Type getRequestType ()
		{
			return requestType;
		}
	}

	// ------------------------------------------------------------------------------
	private class SynchRequestHandler<REQ_PARAM extends JsonSerializable, RESP_PARAM extends JsonSerializable>
			extends RequestHandler<REQ_PARAM, RESP_PARAM>
	{
		public final Function<REQ_PARAM, RESP_PARAM> method;
		public final CheckedFunction<REQ_PARAM, RESP_PARAM> checkedMethod;

		public SynchRequestHandler (Function<REQ_PARAM, RESP_PARAM> method, Type requestType)
		{
			super (requestType);
			this.method = method;
			this.checkedMethod = null;
		}

		public SynchRequestHandler (CheckedFunction<REQ_PARAM, RESP_PARAM> method, Type requestType)
		{
			super (requestType);
			this.method = null;
			this.checkedMethod = method;
		}

		@Override
		public void processRequest (ServiceMessage<REQ_PARAM> request,
				Consumer<ServiceResponse<RESP_PARAM>> responseHandler)
		{
			try {
				if (method != null) {
					responseHandler.accept (new BasicServiceResponse<RESP_PARAM> (method.apply (request.getValue ())));
				}
				else {
					responseHandler
							.accept (new BasicServiceResponse<RESP_PARAM> (checkedMethod.apply (request.getValue ())));
				}
			}
			catch (Throwable thr) {
				responseHandler.accept (new BasicServiceResponse<RESP_PARAM> (thr));
			}
		}

	}

	// ------------------------------------------------------------------------------
	private class AsynchRequestHandler<REQ_PARAM extends JsonSerializable, RESP_PARAM extends JsonSerializable>
			extends RequestHandler<REQ_PARAM, RESP_PARAM>
	{
		public final Consumer<ServiceRequest<REQ_PARAM, RESP_PARAM>> handler;

		public AsynchRequestHandler (Consumer<ServiceRequest<REQ_PARAM, RESP_PARAM>> handler, Type requestType)
		{
			super (requestType);
			this.handler = handler;
		}

		@Override
		public void processRequest (ServiceMessage<REQ_PARAM> request,
				Consumer<ServiceResponse<RESP_PARAM>> responseHandler)
		{
			ReactiveServiceRequest<REQ_PARAM, RESP_PARAM> req = new ReactiveServiceRequest<> (request, responseHandler);
			handler.accept (req);
		}

	}

	// ------------------------------------------------------------------------------
	private class ReactiveServiceRequest<REQ_PARAM extends JsonSerializable, RESP_PARAM extends JsonSerializable>
			extends ServiceRequest<REQ_PARAM, RESP_PARAM>
	{
		private Consumer<ServiceResponse<RESP_PARAM>> handler;

		public ReactiveServiceRequest (ServiceMessage<REQ_PARAM> request, Consumer<ServiceResponse<RESP_PARAM>> handler)
		{
			this (request.getCommand (), request.getValue (), handler);
		}

		public ReactiveServiceRequest (String command, REQ_PARAM parameters,
				Consumer<ServiceResponse<RESP_PARAM>> handler)
		{
			super (command, parameters);
			this.handler = handler;
		}

		@Override
		public ServiceRequest<REQ_PARAM, RESP_PARAM> respond (Single<RESP_PARAM> asyncResponse)
		{
			asyncResponse.subscribeOn (scheduler ()).subscribe (r -> respond (r), t -> respond (t));
			return this;
		}

		@Override
		public ServiceRequest<REQ_PARAM, RESP_PARAM> respond (ServiceResponse<RESP_PARAM> response)
		{
			handler.accept (response);
			return this;
		}

	}
}
