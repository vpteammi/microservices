package com.ptc.microservices.reactive;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import com.ptc.microservices.ServiceException;

import io.reactivex.Scheduler;
import io.reactivex.Single;

public class ReactiveClient<T extends ReactiveClient<?>>
{
	private IReactiveConnector connection;

	protected ReactiveClient (IReactiveConnector connection)
	{
		this.connection = connection;
	}

	@SuppressWarnings ("unchecked")
	public T timeout (long timeout)
	{
		return (T) this;
	}

	@SuppressWarnings ("unchecked")
	public T onFail (Consumer<Throwable> handler)
	{
		return (T) this;
	}

	@SuppressWarnings ("unchecked")
	public T onTimeout (Runnable handler)
	{
		return (T) this;
	}

	@SuppressWarnings ("unchecked")
	public T reply (ServiceResponse<?> connection)
	{
		Class<T> cls = (Class<T>) getClass ();
		try {
			Constructor<?> constructor = cls.getConstructor (IReactiveConnector.class);
			return (T) constructor.newInstance (connection.connector ());
		}
		catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace ();
		}
		return (T) this;
	}

	public void release ()
	{
		connection = null;
	}

	protected <Q extends JsonSerializable, R extends JsonSerializable> void send (String methodName, Q query,
			Consumer<ServiceResponse<R>> handler, Class<R> cls)
	{
		connection.send (new ServiceRequest<Q, R> (methodName, query), handler, cls);
	}

	protected <Q extends JsonSerializable, R extends JsonSerializable> Single<R> sendRx (String methodName, Q query,
			Class<R> cls)
	{
		return getSingle (query, (msg, responseHandler) -> send (methodName, msg, responseHandler, cls));
	}

	protected <Q extends JsonSerializable, R extends JsonSerializable> void send (String methodName, Q query,
			Consumer<ServiceResponse<R>> handler, Type type)
	{
		connection.send (new ServiceRequest<Q, R> (methodName, query), handler, type);
	}

	protected <Q extends JsonSerializable, R extends JsonSerializable> Single<R> sendRx (String methodName, Q query,
			Type type)
	{
		return getSingle (query, (msg, responseHandler) -> send (methodName, msg, responseHandler, type));
	}

	protected <Q extends JsonSerializable, R extends JsonSerializable> void send (String methodName, Q query,
			Consumer<ServiceResponse<R>> handler, Function<String, ServiceResponse<R>> reader)
	{
		connection.send (new ServiceRequest<Q, R> (methodName, query), handler,
				(Function<String, ServiceResponse<R>>) reader);
	}

	protected <Q extends JsonSerializable, R extends JsonSerializable> Single<R> sendRx (String methodName, Q query,
			Function<String, ServiceResponse<R>> reader)
	{
		return getSingle (query, (msg, responseHandler) -> send (methodName, msg, responseHandler, reader));
	}

	protected class SynchConsumer<D extends JsonSerializable> implements Consumer<ServiceResponse<D>>
	{
		private CompletableFuture<ServiceResponse<D>> future;

		@Override
		public void accept (ServiceResponse<D> res)
		{
			if (res.succeeded ()) {
				future.complete (res);
			}
			else {
				future.completeExceptionally (res.cause ());
			}
		}

		private void execute (Consumer<SynchConsumer<D>> executor)
		{
			future = new CompletableFuture<> ();
			executor.accept (this);
		}

		public ServiceResponse<D> getResponse (Consumer<SynchConsumer<D>> executor)
				throws ServiceException, InterruptedException
		{
			execute (executor);
			return getResponse ();
		}

		public CompletableFuture<ServiceResponse<D>> getFutureResponse (Consumer<SynchConsumer<D>> executor)
		{
			execute (executor);
			return future;
		}

		public Single<ServiceResponse<D>> getSingleResponse (Consumer<SynchConsumer<D>> executor)
		{
			return IReactiveConnector.futureToSingle ( () -> getFutureResponse (executor)).subscribeOn (scheduler ());
		}

		public ServiceResponse<D> getResponse () throws ServiceException, InterruptedException
		{
			try {
				return future.get ();
			}
			catch (ExecutionException e) {
				throw new ServiceException (e.getCause ());
			}
		}
	}

	public <D extends JsonSerializable> Single<D> getSingle (Consumer<SynchConsumer<D>> executor)
	{
		return getResponseSingle (executor).map (response -> response.value ());
	}

	public <Q extends JsonSerializable, A extends JsonSerializable> Single<A> getSingle (Q request,
			BiConsumer<Q, Consumer<ServiceResponse<A>>> executor)
	{
		return getSingle (c -> executor.accept (request, c));
	}

	public <D extends JsonSerializable> Single<ServiceResponse<D>> getResponseSingle (
			Consumer<SynchConsumer<D>> executor)
	{
		return new SynchConsumer<D> ().getSingleResponse (executor);
	}

	public <Q extends JsonSerializable, A extends JsonSerializable> Single<ServiceResponse<A>> getResponseSingle (
			Q request, BiConsumer<Q, Consumer<ServiceResponse<A>>> executor)
	{
		return getResponseSingle (c -> executor.accept (request, c));
	}

	public <D extends JsonSerializable> CompletableFuture<D> getFuture (Consumer<SynchConsumer<D>> executor)
	{
		return getResponseFuture (executor).thenApply (response -> response.value ());
	}

	public <Q extends JsonSerializable, A extends JsonSerializable> CompletableFuture<A> getFuture (Q request,
			BiConsumer<Q, Consumer<ServiceResponse<A>>> executor)
	{
		return getFuture (c -> executor.accept (request, c));
	}

	public <D extends JsonSerializable> CompletableFuture<ServiceResponse<D>> getResponseFuture (
			Consumer<SynchConsumer<D>> executor)
	{
		return new SynchConsumer<D> ().getFutureResponse (executor);
	}

	public <Q extends JsonSerializable, A extends JsonSerializable> CompletableFuture<ServiceResponse<A>> getResponseFuture (
			Q request, BiConsumer<Q, Consumer<ServiceResponse<A>>> executor)
	{
		return getResponseFuture (c -> executor.accept (request, c));
	}

	public <D extends JsonSerializable> D synchronize (Consumer<SynchConsumer<D>> executor)
			throws ServiceException, InterruptedException
	{
		return synchronizeResponse (executor).value ();
	}

	public <Q extends JsonSerializable, A extends JsonSerializable> A synchronize (Q request,
			BiConsumer<Q, Consumer<ServiceResponse<A>>> executor) throws ServiceException, InterruptedException
	{
		return synchronize (c -> executor.accept (request, c));
	}

	public <D extends JsonSerializable> ServiceResponse<D> synchronizeResponse (Consumer<SynchConsumer<D>> executor)
			throws ServiceException, InterruptedException
	{
		return new SynchConsumer<D> ().getResponse (executor);
	}

	public <Q extends JsonSerializable, A extends JsonSerializable> ServiceResponse<A> synchronizeResponse (Q request,
			BiConsumer<Q, Consumer<ServiceResponse<A>>> executor) throws ServiceException, InterruptedException
	{
		return synchronizeResponse (c -> executor.accept (request, c));
	}

	public Scheduler scheduler ()
	{
		return connection.defaultScheduler ();
	}
}
