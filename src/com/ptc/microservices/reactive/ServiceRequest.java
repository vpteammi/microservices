package com.ptc.microservices.reactive;

import io.reactivex.Single;

public class ServiceRequest<REQUEST extends JsonSerializable, RESPONSE extends JsonSerializable>
{
	private String command;
	private REQUEST parameters;

	protected ServiceRequest ()
	{
		super ();
	}

	public ServiceRequest (String command, REQUEST parameters)
	{
		super ();
		this.command = command;
		this.parameters = parameters;
	}

	public String getCommand ()
	{
		return command;
	}

	public REQUEST data ()
	{
		return parameters;
	}

	public REQUEST getParameters ()
	{
		return parameters;
	}

	public ServiceRequest<REQUEST, RESPONSE> respond (ServiceResponse<RESPONSE> response)
	{
		// do nothing here.
		return this;
	}

	public ServiceRequest<REQUEST, RESPONSE> respond (RESPONSE response)
	{
		return respond (new BasicServiceResponse<RESPONSE> (response));
	}

	public ServiceRequest<REQUEST, RESPONSE> respond (Single<RESPONSE> asyncResponse)
	{
		asyncResponse.subscribe (r -> respond (r), t -> respond (t));
		return this;
	}

	public ServiceRequest<REQUEST, RESPONSE> respond (Throwable err)
	{
		return respond (new BasicServiceResponse<RESPONSE> (err));
	}
}
