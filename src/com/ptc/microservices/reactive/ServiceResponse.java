package com.ptc.microservices.reactive;

import java.lang.reflect.InvocationTargetException;

import com.sm.javax.query.Result;

public abstract class ServiceResponse<T extends JsonSerializable> extends Result<T>
{
	protected ServiceResponse ()
	{
		super ();
	}

	public ServiceResponse (Throwable cause)
	{
		super (cause);
	}

	public ServiceResponse (T value)
	{
		super (value);
	}

	public abstract IReactiveConnector connector ();

	public abstract ServiceResponse<T> setConnector (IReactiveConnector connector);

	public <API extends ReactiveClient<API>> API api (Class<API> apiClass)
	{
		try {
			return apiClass.getConstructor (IReactiveConnector.class).newInstance (connector ());
		}
		catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new IllegalArgumentException (e);
		}
	}
}
