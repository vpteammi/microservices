package com.ptc.microservices.reactive;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import io.reactivex.Scheduler;
import io.reactivex.Single;

public interface IReactiveConnector extends Closeable
{
	public <Q extends JsonSerializable, R extends JsonSerializable> void send (ServiceRequest<Q, R> query,
			Consumer<ServiceResponse<R>> handler, Class<R> cls);

	public <Q extends JsonSerializable, R extends JsonSerializable> void send (ServiceRequest<Q, R> query,
			Consumer<ServiceResponse<R>> handler, Type type);

	public <Q extends JsonSerializable, R extends JsonSerializable> void send (ServiceRequest<Q, R> query,
			Consumer<ServiceResponse<R>> handler, Function<String, ServiceResponse<R>> reader);

	public <Q extends JsonSerializable, R extends JsonSerializable> void publish (ServiceRequest<Q, R> query);

	@Override
	default void close () throws IOException
	{
		disconnect ();
	}

	public void disconnect ();

	public long getTimeout ();

	public IReactiveConnector setTimeout (long timeoutMsec);

	public <T> CompletableFuture<T> newCompletableFuture ();

	public <T> CompletableFuture<T> newCompletableFuture (Supplier<T> supplier);

	public <T> CompletableFuture<T> newCompletableFuture (Supplier<T> supplier, Executor executor);

	static <T> Single<T> futureToSingle (Supplier<CompletableFuture<T>> cfs)
	{
		return Single.<T> create (emitter -> {
			cfs.get ().whenComplete ( (res, exc) -> {
				if (res != null) {
					emitter.onSuccess (res);
				}
				else {
					emitter.onError (exc);
				}
			});
		});
	}

	static <T> CompletableFuture<T> subscribe (Single<T> single)
	{
		return subscribe (single, null);
	}

	static <T> CompletableFuture<T> subscribe (Single<T> single, Consumer<? super T> onNext)
	{
		CompletableFuture<T> future = new CompletableFuture<T> ();
		single.subscribe (value -> {
			if (onNext != null)
				onNext.accept (value);
			future.complete (value);
		}, ex -> future.completeExceptionally (ex));
		return future;
	}

	public Scheduler defaultScheduler ();
}
