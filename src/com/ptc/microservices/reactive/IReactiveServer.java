package com.ptc.microservices.reactive;

import java.util.concurrent.CompletableFuture;

import com.sm.javax.langx.annotation.SettingProperty;

import io.reactivex.Scheduler;
import io.reactivex.Single;

public interface IReactiveServer
{
	@SettingProperty (defaultValue = "0", cmdLineName = "reactivewebport", envVariableName = "REACTIVE_WEB_PORT", documentation = "For reactive web services running as web service.")
	static int HttpPort = 0;

	public default <INTERFACE> IReactiveServer registerEventBusService (Class<INTERFACE> interfaceClass,
			ReactiveService serviceInstance)
	{
		return registerEventBusService (interfaceClass.getName (), serviceInstance);
	}

	public IReactiveServer registerEventBusService (String serviceName, ReactiveService serviceInstance);

	public default <INTERFACE> IReactiveServer registerWebService (Class<INTERFACE> interfaceClass, int port, String path,
			String debugPath, ReactiveService serviceInstance)
	{
		return registerWebService (interfaceClass.getName (), port, debugPath, serviceInstance);
	}

	public IReactiveServer registerWebService (String serviceName, int port, String path, String debugPath,
			ReactiveService serviceInstance);

	public default IReactiveServer registerWebService (String serviceName, int port, String path,
			ReactiveService serviceInstance)
	{
		return registerWebService (serviceName, port, path, null, serviceInstance);
	}

	public default IReactiveServer registerWebService (String serviceName, String path, ReactiveService serviceInstance)
	{
		return registerWebService (serviceName, HttpPort, path, null, serviceInstance);
	}

	public default <INTERFACE> IReactiveServer registerWebService (Class<INTERFACE> interfaceClass, int port, String path,
			ReactiveService serviceInstance)
	{
		return registerWebService (interfaceClass.getName (), port, path, null, serviceInstance);
	}

	public default <INTERFACE> IReactiveServer registerWebService (Class<INTERFACE> interfaceClass, String path,
			ReactiveService serviceInstance)
	{
		return registerWebService (interfaceClass.getName (), path, serviceInstance);
	}

	public default IReactiveServer registerWebService (String serviceName, ReactiveService serviceInstance)
	{
		return registerWebService (serviceName, "/", serviceInstance);
	}

	public default <INTERFACE> IReactiveServer registerWebService (Class<INTERFACE> interfaceClass,
			ReactiveService serviceInstance)
	{
		return registerWebService (interfaceClass.getName (), serviceInstance);
	}

	public abstract CompletableFuture<Void> start ();

	public abstract Scheduler scheduler ();

	public default Single<Void> startRx ()
	{
		return Single.fromFuture (start (), scheduler ());
	}
}
