package com.ptc.microservices.reactive;

import java.lang.reflect.Type;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.ptc.microservices.IRequestProcessor;
import com.ptc.microservices.reactive.ReactiveService.RequestHandler;
import com.sm.javax.langx.Strings;

/*
 * The main purpose of ReactiveStringRequestProcessor is to process requests in order to 
 * perform executeRequest() method for Abstract REST Service. The main purpose of this class
 * is to implement 
 *   executeRequest (String request, Handler<String> responseHandler)
 * method. That is, a method that takes a string request from a client and after processing 
 * the request executes the given handler providing a string representation of the result.  
 */
public class ReactiveStringRequestProcessor implements IRequestProcessor<String, String>
{
	private ReactiveService serviceInstance;
	private Gson gson;
	private JsonParser parser;

	public ReactiveStringRequestProcessor (Gson gson, ReactiveService serviceInstance)
	{
		super ();
		this.serviceInstance = serviceInstance;
		parser = new JsonParser ();
		this.gson = (gson != null ? gson : RequestResponseSerializer.GetDefaultGson ());
	}

	public ReactiveStringRequestProcessor ()
	{
		this (RequestResponseSerializer.GetDefaultGson (), null);
	}

	public void setServiceInstance (ReactiveService serviceInstance)
	{
		this.serviceInstance = serviceInstance;
	}

	private <RESP_PARAM extends JsonSerializable> ServiceMessage<RESP_PARAM> toServiceMessage (
			ServiceResponse<RESP_PARAM> response)
	{
		ServiceMessage<RESP_PARAM> message = null;
		if (response.succeeded ()) {
			message = new ServiceMessage<RESP_PARAM> (response.value ());
		}
		else {
			message = new ServiceMessage<> (response.cause ());
		}
		return message;
	}

	// ----------------------------------------------------------------------------------
	@Override
	public void executeRequest (String cmd, Consumer<String> responseHandler)
	{
		if (serviceInstance != null) {
			try {
				if (Strings.isEmpty (cmd)) {
					responseHandler.accept ("Service is alive");
					return;
				}
				JsonElement json = parser.parse (cmd);
				if (!json.isJsonObject ()) {
					responseHandler.accept (buildFailMessage ("Failed to parse request '" + cmd + "' to JSON object."));
					return;
				}
				JsonElement commandObj = json.getAsJsonObject ().get ("command");
				if (commandObj == null) {
					responseHandler.accept (buildFailMessage ("Request command is not provided"));
					return;
				}
				String command = commandObj.getAsString ();
				if (Strings.isEmpty (command)) {
					responseHandler.accept (buildFailMessage ("Request command is not provided"));
					return;
				}
				RequestHandler<?, ?> commandHandler = serviceInstance.getRequestHandler (command);
				if (commandHandler == null) {
					if (command.equalsIgnoreCase ("ping")) {
						responseHandler.accept ("Service is alive");
						return;
					}
					responseHandler.accept (buildFailMessage ("Method " + command + " is not available on server"));
					return;
				}
				Type requestType = commandHandler.getRequestType ();
				commandHandler.processRequest (gson.fromJson (json, requestType),
						response -> responseHandler.accept (gson.toJson (toServiceMessage (response))));
			}
			catch (Throwable ex) {
				ex.printStackTrace (System.out);
				responseHandler.accept (buildFailMessage (ex));
			}
		}
	}

	private String buildFailMessage (Throwable ex)
	{
		ServiceMessage<?> msg = new ServiceMessage<> (ex);
		return gson.toJson (msg);
	}

	private String buildFailMessage (String errMsg)
	{
		return buildFailMessage (new Exception (errMsg));
	}

}
