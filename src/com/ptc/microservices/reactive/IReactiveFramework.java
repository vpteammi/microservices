package com.ptc.microservices.reactive;

import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import com.google.gson.Gson;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public interface IReactiveFramework
{
	public default <T> Disposable waitAndShutdown (Observable<CompletableFuture<T>> observable)
	{
		observable.toList ().subscribe (v -> CompletableFuture.allOf (v.toArray (new CompletableFuture [v.size ()]))
				.whenCompleteAsync ( (l, e) -> shutdown ()));
		return Disposables.empty ();
	}

	public void shutdown ();

	// -------------------------------------------------------------------
	public <API extends ReactiveClient<API>> void connectToServer (String serviceName, Class<API> apiClass,
			Consumer<API> connectionHandler, Consumer<Throwable> failHandler)
			throws NoSuchMethodException, SecurityException;

	public default <API extends ReactiveClient<API>> void connectToServer (Class<API> apiClass,
			Consumer<API> connectionHandler, Consumer<Throwable> failHandler)
			throws NoSuchMethodException, SecurityException
	{
		connectToServer (apiClass.getName (), apiClass, connectionHandler, failHandler);
	}

	// -------------------------------------------------------------------
	public default <API extends ReactiveClient<API>> CompletableFuture<API> connectToServer (String serviceName,
			Class<API> apiClass)
	{
		CompletableFuture<API> future = new CompletableFuture<API> ();
		try {
			connectToServer (serviceName, apiClass, api -> future.complete (api),
					ex -> future.completeExceptionally (ex));
		}
		catch (NoSuchMethodException | SecurityException e) {
			future.completeExceptionally (e);
		}
		return future;
	}

	public default <API extends ReactiveClient<API>> CompletableFuture<API> connectToServer (Class<API> apiClass)
	{
		return connectToServer (apiClass.getName (), apiClass);
	}

	// -------------------------------------------------------------------
	public default <API extends ReactiveClient<API>> Single<API> connectToServerRx (String serviceName,
			Class<API> apiClass)
	{
		return IReactiveConnector.futureToSingle ( () -> connectToServer (serviceName, apiClass));
	}

	// -------------------------------------------------------------------
	public default <API extends ReactiveClient<API>> Single<API> connectToServerRx (String serviceName,
			Class<API> apiClass, int retries, long delay, TimeUnit unit)
	{

		return (retries > 0) ? connectToServerRx (apiClass.getName (), apiClass)
				.onErrorResumeNext (ex -> Single.just (retries - 1).delay (delay, unit).flatMap (lessRetries -> {
					return connectToServerRx (serviceName, apiClass, lessRetries, delay, unit);
				})) : connectToServerRx (serviceName, apiClass);
	}

	// -------------------------------------------------------------------
	public default <API extends ReactiveClient<API>> Single<API> connectToServerRx (Class<API> apiClass)
	{
		return connectToServerRx (apiClass.getName (), apiClass);
	}

	// -------------------------------------------------------------------
	public default <API extends ReactiveClient<API>> Single<API> connectToServerRx (Class<API> apiClass, int retries,
			long delay, TimeUnit unit)
	{
		return connectToServerRx (apiClass.getName (), apiClass, retries, delay, unit);
	}

	public IReactiveServer createServer (String[] args, Gson gson);

	public default IReactiveServer createServer (String[] args)
	{
		return createServer (args, null);
	}

	public Scheduler scheduler ();

	@SuppressWarnings ("unchecked")
	public static IReactiveFramework forName (String className)

	{
		IReactiveFramework framework = FRAMEWORKS.map.get (className);
		if (framework == null) {
			try {
				framework = ((Class<IReactiveFramework>) Class.forName (className)).newInstance ();
			}
			catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
				throw new RuntimeException ("Cannot initialize reactive framework '" + className + "'", ex);
			}
			FRAMEWORKS.map.put (className, framework);
		}
		return framework;
	}

	public static IReactiveFramework getDefault ()
	{
		return forName (VERTX);
	}

	static String VERTX = "com.ptc.ccp.vertxservices.reactive.VertxReactiveFramework";

	static class Frameworks
	{
		private HashMap<String, IReactiveFramework> map = new HashMap<> ();
	}

	static Frameworks FRAMEWORKS = new Frameworks ();
}
