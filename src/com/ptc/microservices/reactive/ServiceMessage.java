package com.ptc.microservices.reactive;

import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;

// import com.google.common.reflect.TypeToken;

public class ServiceMessage <VALUE_TYPE extends JsonSerializable> implements JsonSerializable
{
	public static final Type TYPE = new TypeToken<ServiceMessage<JsonSerializable>> () {}.getType ();
	
	private VALUE_TYPE value;
	private Throwable error;
	private String command;
	private Boolean success;
	
	public ServiceMessage ()
	{
	}

	public ServiceMessage (VALUE_TYPE value)
	{
		super ();
		this.value = value;
		this.success = true;
	}	

	public ServiceMessage (Throwable error)
	{
		super ();
		this.error = error;
		this.success = false;
	}	

	public ServiceMessage (String command, VALUE_TYPE value)
	{
		super ();
		this.value = value;
		this.command = command;
	}

	public void setValue (VALUE_TYPE value)
	{
		this.value = value;
		this.success = true;
	}

	public VALUE_TYPE getValue ()
	{
		return value;
	}

	public Throwable getError ()
	{
		return error;
	}

	public String getCommand ()
	{
		return command;
	}

	public boolean getSuccess ()
	{
		return (success == null || success);
	}
}
