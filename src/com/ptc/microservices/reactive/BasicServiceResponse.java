package com.ptc.microservices.reactive;

public class BasicServiceResponse<T extends JsonSerializable> extends ServiceResponse<T>
{

	protected IReactiveConnector connector;
	
	public BasicServiceResponse ()
	{
		super ();
	}
	
	public BasicServiceResponse (Throwable cause)
	{
		super (cause);
	}

	public BasicServiceResponse (T value)
	{
		super (value);
	}

	public BasicServiceResponse (ServiceMessage<T> message, IReactiveConnector connector)
	{
		this.connector = connector;
		if (message.getSuccess ())
		{
			setValue (message.getValue ());
		}
		else
		{
			setFailure (message.getError ());
		}
	}
	
	@Override
	public IReactiveConnector connector ()
	{
		return connector;
	}
	
	public ServiceResponse<T> setConnector (IReactiveConnector connector)
	{
		this.connector = connector;
		return this;
	}

}
