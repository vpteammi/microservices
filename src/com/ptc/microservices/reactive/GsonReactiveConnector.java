package com.ptc.microservices.reactive;

import java.lang.reflect.Type;
import java.util.function.Consumer;
import java.util.function.Function;

import com.google.gson.Gson;

public abstract class GsonReactiveConnector implements IReactiveConnector
{

	protected final Runnable disconnector; 
	protected final Gson     gson;
	protected long           timeout;
	
	public GsonReactiveConnector (Gson gson, Runnable disconnector)
	{
		super ();
		this.gson = gson;
		this.disconnector = disconnector;
	}

	@Override
	public void disconnect ()
	{
		if (disconnector != null) {
			disconnector.run ();
		}
	}

	@Override
	public long getTimeout ()
	{
		return timeout;
	}

	@Override
	public IReactiveConnector setTimeout (long timeoutMsec)
	{
		timeout = timeoutMsec;
		return this;
	}

	private <Q extends JsonSerializable, R extends JsonSerializable> String toJsonMessage (ServiceRequest<Q,R> request)
	{
		return gson.toJson (new ServiceMessage <Q> (request.getCommand (), request.getParameters ()));
	}
	
	protected <R extends JsonSerializable> ServiceResponse<R> toResponse (ServiceMessage<R> message)
	{
		return (new BasicServiceResponse<R> (message, this));
	}
	
	@Override
	public <Q extends JsonSerializable, R extends JsonSerializable> void publish (ServiceRequest<Q,R> request)
	{
		publishRequest (toJsonMessage (request));
	}
	
	protected void publishRequest (String request)
	{
		sendRequest (request, null, (Class<?>) null);
	}
	
	@Override
	public <Q extends JsonSerializable, R extends JsonSerializable> void send (
			ServiceRequest<Q,R> request, Consumer<ServiceResponse<R>> handler, Class<R> responseCls)
	{
		sendRequest (toJsonMessage (request), handler, responseCls);
	}

	protected abstract <RESPONSE extends JsonSerializable> void sendRequest (String request, 
			Consumer<ServiceResponse<RESPONSE>> handler, Class<RESPONSE> responseCls);

	@Override
	public <Q extends JsonSerializable, R extends JsonSerializable> void send (
			ServiceRequest<Q,R> request, Consumer<ServiceResponse<R>> handler, Type type)
	{
		sendRequest (toJsonMessage (request), handler, type);
	}

	protected abstract <RESPONSE extends JsonSerializable> void sendRequest (String request, 
			Consumer<ServiceResponse<RESPONSE>> handler, Type type);
	
	@Override
	public <Q extends JsonSerializable, R extends JsonSerializable> void send (
			ServiceRequest<Q,R> request, Consumer<ServiceResponse<R>> handler, 
			Function<String, ServiceResponse<R>> reader)
	{
		sendRequest (toJsonMessage (request), handler, reader);
	}

	protected abstract <RESPONSE extends JsonSerializable> void sendRequest (String request, 
			Consumer<ServiceResponse<RESPONSE>> handler, Function<String, ServiceResponse<RESPONSE>> reader);
}
