package com.ptc.microservices.reactive;

import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;

// import com.google.common.reflect.TypeToken;

public interface JsonSerializable
{
	public static class MessageType <REQUEST extends JsonSerializable> extends TypeToken<ServiceMessage<REQUEST>>
	{
	}
	
	public default String toJson (Type type) {
		return RequestResponseSerializer.GetDefaultGson ().toJson (this, type);
	}
	
	public default String toJson () {
		return RequestResponseSerializer.GetDefaultGson ().toJson (this);
	}
}
