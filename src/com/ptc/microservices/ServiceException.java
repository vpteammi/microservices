package com.ptc.microservices;

public class ServiceException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4008540648278068624L;

	public ServiceException (String msg)
	{
		super (msg);
	}
	
	public ServiceException (Throwable ex)
	{
		super (ex);
	}
}