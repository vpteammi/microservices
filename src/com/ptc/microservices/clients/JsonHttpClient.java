package com.ptc.microservices.clients;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.function.Consumer;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sm.javax.query.Result;

public class JsonHttpClient extends HttpClient
{
	private URL serverUrl;
	private JsonParser parser;
	private long timeout;
	
	public JsonHttpClient (URL serverUrl, ScheduledThreadPoolExecutor executor, long timeout)
	{
		super (executor);
		this.serverUrl = serverUrl;
		this.parser = new JsonParser ();
		this.timeout = timeout;
	}
	
	public JsonHttpClient (URL serverUrl, long timeout)
	{
		this (serverUrl, ThreadExecutorTools.getPoolExecutor (), timeout);
	}
	
	public JsonHttpClient (URL serverUrl)
	{
		this (serverUrl, ThreadExecutorTools.getPoolExecutor (), 0L);
	}
	
	// ----------------------------------------------------------------
	public void sendPostRequest (JsonObject request, Consumer<Result<JsonObject>> handler, long timeout)
	{
		try {
			this
				.post (serverUrl, res -> processResponse (res, handler), timeout)
				.putHeader (CONTENT_TYPE, "text/plain")
				.putHeader ("User-Agent", DefaultUserAgent)
				.putHeader ("Accept-Language", "en-US,en;q=0.5")
				.write (request.toString ())
				.end ();
		}
		catch (IOException e) {
			handler.accept (new Result<JsonObject> (e));
		}
	}
	
	public void sendPostRequest (JsonObject request, Consumer<Result<JsonObject>> handler)
	{
		sendPostRequest (request, handler, timeout);
	}
	
	// ----------------------------------------------------------------
	public void sendGetRequest (JsonObject request, Consumer<Result<JsonObject>> handler, long timeout)
	{
		try {
			this
				.get (serverUrl, res -> processResponse (res, handler), timeout)
				.putHeader (CONTENT_TYPE, "text/plain")
				.putHeader ("User-Agent", DefaultUserAgent)
				.putHeader ("Accept-Language", "en-US,en;q=0.5")
				.write (request.toString ())
				.end ();
		}
		catch (IOException e) {
			handler.accept (new Result<JsonObject> (e));
		}
	}

	public void sendGetRequest (JsonObject request, Consumer<Result<JsonObject>> handler)
	{
		sendGetRequest (request, handler, timeout);
	}
	
	// ----------------------------------------------------------------
	private void processResponse (HttpClientResponse res, Consumer<Result<JsonObject>> handler)
	{
		if (handler != null) {
			Result<JsonObject> response = null;
			if (res.succeeded ()) {
				try {
					response = new Result<> (parser.parse (res.getBody ()).getAsJsonObject ());
				}
				catch (Exception ex) {
					response = new Result<> (ex);
				}
			}
			else {
				response = new Result<>(res.cause ());
			}
			handler.accept (response);
		}
	}

}
