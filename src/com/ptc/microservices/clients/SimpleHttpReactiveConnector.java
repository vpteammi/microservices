package com.ptc.microservices.clients;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.google.gson.Gson;
import com.ptc.microservices.reactive.BasicServiceResponse;
import com.ptc.microservices.reactive.GsonReactiveConnector;
import com.ptc.microservices.reactive.IReactiveConnector;
import com.ptc.microservices.reactive.JsonSerializable;
import com.ptc.microservices.reactive.ReactiveClient;
import com.ptc.microservices.reactive.RequestResponseSerializer;
import com.ptc.microservices.reactive.ServiceMessage;
import com.ptc.microservices.reactive.ServiceResponse;
import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.utilx.ApplicationProperties;
import com.sm.javax.utilx.Tuple2;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class SimpleHttpReactiveConnector extends GsonReactiveConnector
{
	@SettingProperty (defaultValue="2000", envVariableName="HTTP_WAIT_RESPONSE_TIMEOUT", documentation="Default HTTP timeout (msec)")
	static long DefaultHttpTimeoutMS;

	protected final URL serverUrl;
	protected String    userAgent;
	protected String    contentType;
	private HttpClient  client;

	public SimpleHttpReactiveConnector (URL serverUrl, Runnable disconnector, Gson gson)
	{
		super (gson, disconnector);
		this.serverUrl = serverUrl;
		ApplicationProperties.processStaticProperties (getClass ());
		userAgent = HttpClient.DefaultUserAgent;
		contentType = HttpClient.DefaultContentType;
		timeout = DefaultHttpTimeoutMS;
		client = new HttpClient (new ScheduledThreadPoolExecutor (12));
	}

	public SimpleHttpReactiveConnector (String serverUrlAddr, Runnable disconnector, Gson gson) throws MalformedURLException
	{
		this (new URL (serverUrlAddr), disconnector, gson);
	}

	public SimpleHttpReactiveConnector (String serverUrlAddr, Runnable disconnector) throws MalformedURLException
	{
		this (serverUrlAddr, disconnector, RequestResponseSerializer.GetDefaultGson ());
	}

	// -------------------------------------------------------------------------------------
	public static <API extends ReactiveClient<API>> void connectToServer (String serviceName, Class<API> apiClass,
			Consumer<API> connectionHandler, Consumer<Throwable> failHandler)
			throws NoSuchMethodException, SecurityException
	{
		Constructor<API> apiConstructor = apiClass.getConstructor (IReactiveConnector.class);
		connectToServer (serviceName, (args) -> {
				API apiClient = null; 
				try {
					apiClient = apiConstructor.newInstance (
							new SimpleHttpReactiveConnector (args._1 /*address*/, args._2 /*disconnector*/));
					return apiClient;
				} 
				catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | MalformedURLException e) {
					failHandler.accept (e);
				}
				return apiClient;
			}, connectionHandler, failHandler);
	}
	
	public static <API extends ReactiveClient<API>> void connectToServer (String url, Function<Tuple2<String, Runnable>, API> clientCreator,
			Consumer<API> connectionHandler, Consumer<Throwable> failHandler)
	{
		API apiClient = clientCreator.apply (new Tuple2<String, Runnable> (url, null));
		connectionHandler.accept (apiClient);
	}
	
	// -------------------------------------------------------------------------------------
	@SuppressWarnings ("unchecked")
	@Override
	protected <RESPONSE extends JsonSerializable> void sendRequest (String request,
			Consumer<ServiceResponse<RESPONSE>> handler, Class<RESPONSE> responseCls)
	{
		sendRequest (request, handler, body -> toResponse ((ServiceMessage<RESPONSE>) gson.fromJson (body, responseCls)));
	}

	@SuppressWarnings ("unchecked")
	@Override
	protected <RESPONSE extends JsonSerializable> void sendRequest (String request,
			Consumer<ServiceResponse<RESPONSE>> handler, Type type)
	{
		sendRequest (request, handler, 
				body -> toResponse ((ServiceMessage<RESPONSE>) gson.fromJson (body, type)));
	}

	@Override
	protected <RESPONSE extends JsonSerializable> void sendRequest (String request,
			Consumer<ServiceResponse<RESPONSE>> handler, Function<String, ServiceResponse<RESPONSE>> reader)
	{
		try {
			client
				.post (serverUrl, res -> processResponse (res, handler, reader), getTimeout ())
				.putHeader (HttpClient.CONTENT_TYPE, "text/plain")
				.putHeader ("User-Agent", userAgent)
				.putHeader ("Accept-Language", "en-US,en;q=0.5")
				.write (request)
				.end ();
		}
		catch (IOException e) {
			handler.accept (new BasicServiceResponse<RESPONSE> (e));
		}
	}

	private <RESPONSE extends JsonSerializable> void processResponse (HttpClientResponse res, 
			Consumer<ServiceResponse<RESPONSE>> handler, Function<String, ServiceResponse<RESPONSE>> reader)
	{
		if (handler != null) {
			ServiceResponse<RESPONSE> response = null;
			if (res.succeeded ()) {
				try {
					response = reader.apply (res.getBody ());
				}
				catch (Exception ex) {
					response = new BasicServiceResponse<> (ex);
				}
			}
			else {
				response = new BasicServiceResponse<>(res.cause ());
			}
			handler.accept (response);
		}
	}

	// -------------------------------------------------------------------------------------
	public String getUserAgent ()
	{
		return userAgent;
	}

	public void setUserAgent (String userAgent)
	{
		this.userAgent = userAgent;
	}

	public String getContentType ()
	{
		return contentType;
	}

	public void setContentType (String contentType)
	{
		this.contentType = contentType;
	}

	@Override
	public <T> CompletableFuture<T> newCompletableFuture ()
	{
		return new CompletableFuture<T> ();
	}

	@Override
	public <T> CompletableFuture<T> newCompletableFuture (Supplier<T> supplier)
	{
		return CompletableFuture.supplyAsync (supplier);
	}
	
	@Override
	public <T> CompletableFuture<T> newCompletableFuture (Supplier<T> supplier, Executor executor)
	{
		return CompletableFuture.supplyAsync (supplier, executor);
	}

	@Override
	public Scheduler defaultScheduler ()
	{
		return Schedulers.computation ();
	}

}
