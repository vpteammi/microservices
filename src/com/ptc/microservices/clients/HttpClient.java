package com.ptc.microservices.clients;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.function.Consumer;

import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.utilx.ApplicationProperties;

public class HttpClient
{
	public static final String CONTENT_LENGTH = "Content-length";
	public static final String CONTENT_TYPE = "Content-Type";
	
	@SettingProperty (defaultValue="Mozilla/5.0", envVariableName="HTTP_CLIENT_USER_AGENT")
	public static String DefaultUserAgent;
	
	@SettingProperty (defaultValue="text/plain", envVariableName="HTTP_CLIENT_CONTENT_TYPE")
	public static String DefaultContentType;
	
	static {
		ApplicationProperties.processStaticProperties (HttpClient.class);
	}
	
	private ScheduledThreadPoolExecutor executor;
	
	public HttpClient (ScheduledThreadPoolExecutor executor)
	{
		super ();
		this.executor = executor;
	}
	
	public HttpClientRequest post (URL serverUrl, Consumer<HttpClientResponse> handler, long timeout) throws IOException
	{
		return (new HttpClientRequest ((HttpURLConnection) serverUrl.openConnection (), handler, executor)
				.setRequestMethod ("POST").setTimeout (timeout));
	}

	public HttpClientRequest post (URL serverUrl, Consumer<HttpClientResponse> handler) throws IOException
	{
		return post (serverUrl, handler, 0L);
	}

	public HttpClientRequest get (URL serverUrl, Consumer<HttpClientResponse> handler, long timeout) throws IOException
	{
		return (new HttpClientRequest ((HttpURLConnection) serverUrl.openConnection (), handler, executor)
				.setRequestMethod ("GET").setTimeout (timeout));
	}

	public HttpClientRequest get (URL serverUrl, Consumer<HttpClientResponse> handler) throws IOException
	{
		return get (serverUrl, handler, 0L);
	}
	
	public void stop ()
	{
		executor.shutdown ();
	}
	
}