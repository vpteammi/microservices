package com.ptc.microservices.clients;

import java.util.concurrent.ScheduledThreadPoolExecutor;

public class ThreadExecutorTools
{
	private static ScheduledThreadPoolExecutor executor;

	public static ScheduledThreadPoolExecutor getPoolExecutor ()
	{
		if (executor == null) {
			executor = new ScheduledThreadPoolExecutor (15);
		}
		return executor;
	}
 
}
