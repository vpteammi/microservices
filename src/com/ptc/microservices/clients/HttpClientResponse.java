package com.ptc.microservices.clients;

import java.util.List;
import java.util.Map;

import com.sm.javax.query.Result;

// -------------------------------------------------------------------------------------
public class HttpClientResponse extends Result<HttpClientRequest>
{
	private int    responseCode;
	private String body;
	private Map<String, List<String>> headerFields;
	
	public HttpClientResponse (HttpClientRequest request, String body)
	{
		super (request);
		setBody (body);
	}

	public HttpClientResponse (int code, String errMsg)
	{
		this (code, errMsg, null);
	}

	public HttpClientResponse (int code, String errMsg, String body)
	{
		super (errMsg);
		this.body = body;
		responseCode = code;
	}

	public HttpClientResponse (Throwable error)
	{
		super (error);
	}
	
	public HttpClientResponse setBody (String body)
	{
		this.body = body;
		responseCode = 200;
		return this;
	}
	
	public HttpClientResponse setCode (int code)
	{
		this.responseCode = code;
		return this;
	}

	public int getResponseCode ()
	{
		return responseCode;
	}
	
	public int statusCode ()
	{
		return getResponseCode ();
	}

	public String getBody ()
	{
		return body;
	}

	public HttpClientResponse setHeaders (Map<String, List<String>> headerFields)
	{
		this.headerFields = headerFields;
		return this;
	}
	
	public List<String> getHeader (String headerTitle)
	{
		if (headerFields != null) {
			return headerFields.get (headerTitle);
		}
		return null;
	}
}