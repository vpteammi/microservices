package com.ptc.microservices.clients;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.util.Base64;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import com.sm.javax.langx.Strings;

public class HttpClientRequest 
{
	
	public static final String ContentLength = "Content-Length";
	public static final String ContentType = "Content-type";
	
	public static final String JsonType = "application/json";
	public static final String ZipType = "application/zip"; 
	
	public enum RequestMethod {
		GET, 
		POST, 
		HEAD, 
		OPTIONS, 
		PUT,
		DELETE, 
		TRACE 
	};
	
	// ----------------------------------------------------------------------------
	private HttpURLConnection connection;
	private Consumer<HttpClientResponse> responseHandler;
	private ByteArrayOutputStream buffer;
	private Throwable error;
	private ScheduledThreadPoolExecutor executor;
	private long timeout;
	private ScheduledFuture<?> terminatorTask;
	private Future<?> processingTask;

	public HttpClientRequest (HttpURLConnection connection, Consumer<HttpClientResponse> responseHandler, 
			ScheduledThreadPoolExecutor executor)
	{
		super ();
		this.connection = connection;
		this.responseHandler = responseHandler;
		this.timeout = SimpleHttpReactiveConnector.DefaultHttpTimeoutMS;
		this.buffer = new ByteArrayOutputStream ();
		this.executor = executor;
		this.error = null;
	}
	
	public HttpClientRequest (HttpURLConnection connection, Consumer<HttpClientResponse> responseHandler)
	{
		this (connection, responseHandler, ThreadExecutorTools.getPoolExecutor ());
	}
	
	public static HttpClientRequest createPostResquest (HttpURLConnection connection, Consumer<HttpClientResponse> responseHandler)
	{
		return new HttpClientRequest (connection, responseHandler).setRequestMethod ("POST");
	}
	
	public static HttpClientRequest createGetResquest (HttpURLConnection connection, Consumer<HttpClientResponse> responseHandler)
	{
		return new HttpClientRequest (connection, responseHandler).setRequestMethod ("GET");
	}
	
	public static HttpClientRequest createDeleteResquest (HttpURLConnection connection, Consumer<HttpClientResponse> responseHandler)
	{
		return new HttpClientRequest (connection, responseHandler).setRequestMethod ("DELETE");
	}
	
	// ----------------------------------------------------------------------------
	public static void releaseDefaultExecutor ()
	{
		ThreadExecutorTools.getPoolExecutor ().shutdown ();
	}
	
	public HttpClientRequest setRequestMethod (RequestMethod method)
	{
		return this.setRequestMethod (method.toString ());
	}
	
	public HttpClientRequest setRequestMethod (String method)
	{
		try {
			connection.setRequestMethod (method);
		}
		catch (ProtocolException e) {
			error = e;
		}
		return this;
	}
	
	public HttpClientRequest putHeader (String headerTitle, String headerValue)
	{
		connection.setRequestProperty (headerTitle, headerValue);
		return this;
	}
	
	public HttpClientRequest setTimeout (long timeout)
	{
		this.timeout = timeout;
		return this;
	}
	
	public HttpClientRequest setBasicAuthentication (String login, String password)
	{
		String authorization = String.format ("%s:%s", login, password);
		byte[] authData = null;
		try {
			authData = authorization.getBytes ("UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			authData = authorization.getBytes ();
		}
		String encoding = Base64.getEncoder ().encodeToString (authData);
		connection.setRequestProperty ("Authorization", "Basic " + encoding);
		return this;
	}
	
	public HttpClientRequest write (byte[] data)
	{
		try {
			buffer.write (data);
		}
		catch (IOException e) {
			error = e;
		}
		return this;
	}
	
	public HttpClientRequest write (String msg)
	{
		if (! Strings.isEmpty (msg)) {
			write (msg.getBytes ());
		}
		return this;
	}
	
	public void end (byte[] data)
	{
		boolean isPOST =  "POST".equals (connection.getRequestMethod ());
		if (isPOST) {
			connection.setDoOutput (true);
			write (data);
			if (error != null) {
				processError ();
			}
			else {
				connection.setRequestProperty ("Content-Length", buffer.size() + "");
				try (DataOutputStream requestOut = new DataOutputStream (connection.getOutputStream ())) {
					requestOut.write (buffer.toByteArray());
					requestOut.flush ();
				}
				catch (IOException e) {
					error = e;
				}
				if (error != null) {
					processError ();
				}
				waitForResponse ();
			}
		}
		else {
			waitForResponse ();
		}
	}
	
	private void processError ()
	{
		responseHandler.accept (new HttpClientResponse (error));
	}
	
	private synchronized void terminateResponseProcessing ()
	{
		if (processingTask != null) {
			processingTask.cancel (true);
		}
	}
	
	private void waitForResponse ()
	{
		if (timeout > 0) {
			terminatorTask = executor.schedule (this::terminateResponseProcessing, timeout, TimeUnit.MILLISECONDS);
		}
		processingTask = executor.submit (this::processResponse);
	}
	
	private void cancelTerminationTask ()
	{
		if (terminatorTask != null) {
			terminatorTask.cancel (true);
		}
	}
	
	private synchronized void processResponse ()
	{
		HttpClientResponse response = readResponse ();
		cancelTerminationTask ();
		responseHandler.accept (response);
	}
	
	private HttpClientResponse readResponse ()
	{
		try {
			int responseCode = connection.getResponseCode ();
			cancelTerminationTask ();
			if (responseCode != 200) {
				return readFailedResponse (responseCode);
			}
			BufferedReader in = new BufferedReader (new InputStreamReader (connection.getInputStream ()));
			StringBuffer response = new StringBuffer();
			String output;		 
			while ((output = in.readLine ()) != null) {
				response.append (output);
			}
			in.close ();
			processingTask = null;
			return (new HttpClientResponse (this, response.toString ()).setHeaders (connection.getHeaderFields ()));
		}
		catch (IOException ex) {
			return (new HttpClientResponse (ex));
		}
	}

	private HttpClientResponse readFailedResponse (int responseCode)
	{
		String errMsg = "HTTP request failed";
		int firstDigit = responseCode / 100; 
		String body = null;
		if (firstDigit == 4 || firstDigit == 5) {
			errMsg = (firstDigit == 4 ? "Client Error: " : "Server Error: ");
			try (BufferedReader in = new BufferedReader (new InputStreamReader (connection.getErrorStream ()))) {
				StringBuffer response = new StringBuffer();
				String output;		 
				while ((output = in.readLine ()) != null) {
					response.append (output);
				}
				body = response.toString ();
			}
			catch (IOException e) {
			}
		}
		return (new HttpClientResponse (responseCode, errMsg, body));
	}

	public void end ()
	{
		end (new byte[0]);
	}
	
}