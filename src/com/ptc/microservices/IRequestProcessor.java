package com.ptc.microservices;

import java.util.function.Consumer;

public interface IRequestProcessor<REQ_TYPE, RESP_TYPE>
{
	void executeRequest (REQ_TYPE request, Consumer<RESP_TYPE> responseHandler);
}
